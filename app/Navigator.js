import React from "react";
import { createAppContainer, createBottomTabNavigator } from "react-navigation";
import Icon from "react-native-vector-icons/FontAwesome5";
import HomeView from "./views/HomeView.js";

// Ciação do menu inferior para navegação entre telas
const MainNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: HomeView,
      navigationOptions: {
        tabBarLabel: "Home",
        tabBarIcon: () => <Icon name="igloo" color={"#FA1334"} size={30} />
      }
    }
  },
  {
    initialRouteName: "Home",
    tabBarOptions: {
      activeTintColor: "#FA1334",
      inactiveTintColor: "#FA1334",
      activeBackgroundColor: "#fffffff",
      animationEnabled: true
    }
  }
);

const Main = createAppContainer(MainNavigator);
export default Main;
